package firsthome;

public abstract class AbstractProduct 
{
	private String name;
	private int quantity;

	
	public AbstractProduct(String name, int quantity) {
		super();
		this.name = name;
		this.quantity = quantity;
	}
	
	public AbstractProduct() {
		super();
	}

	public String getName() {
		return name;
	}
	public int getQuantity() {
		return quantity;
	}
	
	public boolean compareWith(AbstractProduct abstractProduct)
	{
		if (this.name.equals(abstractProduct.getName()) &&
				this.getClass() == abstractProduct.getClass())
		{
			return true;
		}
		return false;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void showDetailsAboutProduct()
	{
		System.out.println("Nazwa: " + name + " Ilosc: " + quantity + showDetailsInWhichQuantityStored());
	}
	
	public abstract String showDetailsInWhichQuantityStored();
}
