package firsthome;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sun.org.apache.bcel.internal.generic.ReturnaddressType;

import jdk.internal.org.objectweb.asm.util.CheckAnnotationAdapter;



public class StartingClass {

	public final static String firstOption = "Wpisz \"asortyment\", aby wyswietlic stan magazynu";
	public final static String secondOption = "Wpisz \"dostawa\", aby wpisac nazwe produktu do zmagazynowania.";
	public final static String thirdOption = "Wpisz \"kupno\", aby wpisac nazwe produktu, który chcemy kupić";
	public final static String endOption = "Wpisz \"end\", aby wyjsc z systemu";
	public final static String hintForUserAboutInputOptions = firstOption + "\n"+ secondOption + "\n"+ thirdOption + "\n"+ endOption;

	private final static int naxAmountOfProducts = 30;
	private static List<AbstractProduct> listOfProducts = new ArrayList<AbstractProduct>();
	 
	
	public static void main(String[] args) 
	{
		boolean wantToCloseProgram = false;
		Scanner scanner = new Scanner(System.in);	
		showHintForUser();
		
		do
		{		
			System.out.println("Wpisz swa prosbe:");					
			switch (takeStringFromNextLineOfScanner(scanner))
			{
				case "asortyment":
					showProductsForUser();
					break;
				case "dostawa":
					serveDelivery(scanner);
					break;
				case "kupno":
					servePurchase(scanner);
					break;
				case "end":
					wantToCloseProgram = true;
					break;
			default:
				System.out.println("Pomyliles sie? \nOto dostepne opcje:");
				showHintForUser();
				break;
			}				
			
			
		}while(!wantToCloseProgram);
		scanner.close();
		System.out.println("Do zobaczenia.");	
	}
	
	private static void showHintForUser()
    {
    	System.out.println(hintForUserAboutInputOptions);
    }
          
	private static void showProductsForUser()
	{
    	System.out.println("Lista dostepnych produktów:");
		for (AbstractProduct product : listOfProducts)
		{
			product.showDetailsAboutProduct();
		}
	}
    

	private static void serveDelivery(Scanner scanner)  
	{				
		System.out.println("Proces magazynowania produktu");
		if(isProductDeliveryProcessedSuccessfully(scanner))
		{
			System.out.println("Magazyn zaktualizowano.");
			showProductsForUser();				
		}		
	}

	private static boolean isProductDeliveryProcessedSuccessfully(Scanner scanner) 
	{
		AbstractProduct productToAdd = createObjectFromUserInput(scanner);
		// czy lepiej to rozbic na dwie metody i zwrocic blad za pomoca enuma 
		// czy moze zeby dwa razy nie przeszukiwac danej listy znalezc takie same typy produktow i 
		// i dodac ilosc w srodku metody checkIfProductAlreadyInStorage oczywiscie przy tym zmieniajac nazwe 
		// zeby mowila co w srodku sie dzieje 
		if (checkIfProductAlreadyInStorage(productToAdd))
		{
			if (increasedSuccessfullyAmountOfExistingProduct(productToAdd))
			{
				System.out.println("Zwiekszono stan magazynu o dana ilosc produktu");
				return true;
			}
			System.out.println("Problem z dodaniem produktu.");
			return false;			
		}
		
		if (puttProductSuccessfullyOnFreeSlotIfExists(productToAdd))
		{
			System.out.println("Dodano nowy produkt do magazynu");
			return true;			
		}		
		System.out.println("Brak miejsca w magazynie");
		return false;
	}


	private static AbstractProduct createObjectFromUserInput(Scanner scanner) 
	{	
		String nameOfProduct = getNameOfProduct(scanner);			
		int quantityOfProduct = getQuantityOfProduct(scanner);
		AbstractProduct productToAdd = getIdentifierOfQuantity(scanner);
		productToAdd.setName(nameOfProduct);
		productToAdd.setQuantity(quantityOfProduct);
		
		return productToAdd;
	}

	private static String getNameOfProduct(Scanner scanner) {
		System.out.println("Podaj nazwę produktu");	
		return takeStringFromNextLineOfScanner(scanner);
	}

	private static boolean puttProductSuccessfullyOnFreeSlotIfExists(AbstractProduct productToAdd) {
		if (checkIfThereIsSpaceInStorage())
		{
			listOfProducts.add(productToAdd);
			return true;
		}
		return false;
	}
	
	private static boolean checkIfThereIsSpaceInStorage()
	{
		return listOfProducts.size() < naxAmountOfProducts;
	}

	private static boolean increasedSuccessfullyAmountOfExistingProduct(AbstractProduct productToAdd) 
	{
		for (AbstractProduct abstractProduct : listOfProducts) 
		{
			if (abstractProduct.compareWith(productToAdd))
			{
				abstractProduct.setQuantity(abstractProduct.getQuantity() + productToAdd.getQuantity());
				return true;
			}			
		}	
		return false;
	}

	private static boolean checkIfProductAlreadyInStorage(AbstractProduct productToAdd) {
		
		for (AbstractProduct abstractProduct : listOfProducts) 
		{
			if (abstractProduct.compareWith(productToAdd) == true)
			{ 
				return true;
			}					
		}		
		return false;
	}
	

	private static int getQuantityOfProduct(Scanner scanner) 
	{
		System.out.println("Podaj ilosc produktu (liczba)");
		int iloscProduktu = 0;
		 try  
		 {  
			 iloscProduktu = Integer.parseInt(takeStringFromNextLineOfScanner(scanner));  
		 }  
		 catch(NumberFormatException nfe)  
		 {  
		   iloscProduktu = getQuantityOfProduct(scanner);
		 }	
		 return iloscProduktu;
	}


	private static AbstractProduct getIdentifierOfQuantity(Scanner scanner) {
		System.out.println("Podaj jednostke w jakiej jest magazynowany do wyboru kg/L/szt");
		switch (takeStringFromNextLineOfScanner(scanner))
		{
		case "szt":
			return new ProductByPieces();
		case "kg":
			return new ProductByKG();
		case "L":
			return new ProductByLitres();
			default:		
		}	
		return getIdentifierOfQuantity(scanner);
		
	}
	
	private static void servePurchase(Scanner scanner) 
	{
		System.out.println("Proces kupna produktu");		
		if(isProductPurchaseSuccessfully(scanner))
		{
			System.out.println("Produkt sprzedany.");
		}		
	}

	private static boolean isProductPurchaseSuccessfully(Scanner scanner)
	{
		AbstractProduct detailsOfProductWantedToPurchase = createObjectFromUserInput(scanner);
		
		for (AbstractProduct itemFromStorage : listOfProducts) 
		{
			if ( checkIfProductFromStorageIsTheSameAsDesiredByUser(itemFromStorage, detailsOfProductWantedToPurchase ))
			{
				if (checkIfUserWantMoreAmountOfProductThenStoredInMagazine(itemFromStorage.getQuantity(), detailsOfProductWantedToPurchase.getQuantity()))
				{
					System.out.println("Za malo produktu w magazynie, zeby wykonac operacje");
					return false; 
				}

				int productLeft = itemFromStorage.getQuantity() - detailsOfProductWantedToPurchase.getQuantity();
				if (productLeft == 0 )
				{
					listOfProducts.remove(itemFromStorage);
					return true;
				}
				itemFromStorage.setQuantity(productLeft);	
				return true;
			}			
		}	
		System.out.println("Nie znaleziono zadanego produktu");
		return false;
	}



	
	private static boolean checkIfUserWantMoreAmountOfProductThenStoredInMagazine(int amountOfProductInStorage,
			int amountOfWantedByUser) {
		return amountOfWantedByUser > amountOfProductInStorage;		
	}

	private static boolean checkIfProductFromStorageIsTheSameAsDesiredByUser(AbstractProduct itemFromStorage,
			AbstractProduct detailsOfProductWantedToPurchase) {
		return itemFromStorage.compareWith(detailsOfProductWantedToPurchase);	
	}

	private static String takeStringFromNextLineOfScanner(Scanner scanner)
	{
		return scanner.nextLine().toString().trim();
	}

}
