package liczby_rzymskie;

import java.io.Console;
import java.util.Scanner;

import com.sun.jndi.url.iiopname.iiopnameURLContextFactory;

public class liczbyRzymskie 
{


	 
	
	public static void main(String[] args) 
	{
		boolean wantToCloseProgram = false;
		Scanner scanner = new Scanner(System.in);	
		String gettedText = "";
		
		do
		{
			System.out.println("Wpisz liczbe ktora chcesz zamienic lub wyjdz wpisujac \"end\":");
			gettedText = takeStringFromNextLineOfScanner(scanner);
			if (gettedText.equals("end"))
			{
				wantToCloseProgram = true;
			}
			else 
			{
				calculateValueOfRomanNumberIsString(gettedText.toUpperCase());
			}	
			
			
		}while(!wantToCloseProgram);
		scanner.close();
		System.out.println("Do zobaczenia.");	
	}
	
	
	private static void calculateValueOfRomanNumberIsString(String gettedText) 
	{
		if (!gettedText.matches("^(M{0,4})(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$"))
		{
			System.out.println("zly format");
			return;
		}
		
		
		int arabNumber =0;
		try 
		{
			arabNumber = toArabic(gettedText);
		}
	    catch(calculateRomanNumberException ex)
		{
	    	System.out.println("zly format");
			return;
		}
		
		System.out.println(arabNumber);
	}
	// TODO recursive method concept nice to try this kind of functions 
	public static int toArabic(String number) throws calculateRomanNumberException {
	 if (number.isEmpty()) return 0;
     if (number.startsWith("M")) return 1000 +  toArabic(number.substring(1));
     if (number.startsWith("CM")) return 900 +  toArabic(number.substring (2));
     if (number.startsWith("D")) return 500 +  toArabic(number.substring(1));
     if (number.startsWith("CD")) return 400 +  toArabic(number.substring(2));
     if (number.startsWith("C")) return 100 +  toArabic(number.substring(1));
     if (number.startsWith("XC")) return 90 +  toArabic(number.substring(2));
     if (number.startsWith("L")) return 50 +  toArabic(number.substring(1));
     if (number.startsWith("XL")) return 40 +  toArabic(number.substring(2));
     if (number.startsWith("X")) return 10 +  toArabic(number.substring(1));
     if (number.startsWith("IX")) return 9 +  toArabic(number.substring(2));
     if (number.startsWith("V")) return 5 +  toArabic(number.substring(1));
     if (number.startsWith("IV")) return 4 +  toArabic(number.substring(2));
     if (number.startsWith("I")) return 1 +  toArabic(number.substring(1));
     throw new calculateRomanNumberException();
	}

	private static String takeStringFromNextLineOfScanner(Scanner scanner)
	{
		return scanner.nextLine().toString().trim();
	}

}


