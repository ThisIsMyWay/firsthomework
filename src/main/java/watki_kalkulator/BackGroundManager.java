package watki_kalkulator;


import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import javafx.application.Platform;
import watki_kalkulator.task_classes.TaskManager;

public class BackGroundManager 
{
	UIClass userInterface = null;
	TaskManager taskManager = null;
	

	public BackGroundManager(UIClass userInterface) {
		super();
		this.userInterface = userInterface;
		taskManager = new TaskManager(this);
		setTimer();
	}
	
	Timer timer;
	public void setTimer() 
	{
		timer = new Timer();
		timer.schedule(new ToDrawCounter(5) , 0, 1000);
	}
	
	
	// Single responsiblity vs dużo referencji do przekazania i mała klasa 
	private class ToDrawCounter extends TimerTask
	{
		private int secondsToDraw ;		
		
		public ToDrawCounter(int secondsToDraw) {
			super();
			this.secondsToDraw = secondsToDraw;
		}
		
		@Override
		public void run() 
		{
			Platform.runLater(new Runnable() {
				public void run() {					
					if (secondsToDraw > 0)
					{
						userInterface.updateTimer(secondsToDraw);						
					}
					else if (secondsToDraw == 0)
					{				
						userInterface.updateTimer(0);
					}
					else 
					{
						timer.cancel();					
						taskManager.startCalculation(drawNextOperation());
					}
					secondsToDraw--;
				}
			});
		}
	}
	
	public void setOperationResult(OperarationDataCantainer operarationDataCantainer)
	{
		Platform.runLater(new Runnable() {
			public void run() {
				userInterface.updateNumbers(operarationDataCantainer);
				setTimer();
			}
		});
	}
	
	
	public OperarationDataCantainer drawNextOperation()
	{
		Random random = new Random();
		OperationOption optionRandomized = OperationOption.ADDING;
		optionRandomized =	optionRandomized.getValueByNumber(random.nextInt((OperationOption.maxValue - 0) + 1));
		int randomedNumberForCalculation =  random.nextInt((40 - 0) + 1);
		return new OperarationDataCantainer(optionRandomized, randomedNumberForCalculation, userInterface.getSumNumber());
	}
	
	public void stopApplicationTasks()
	{
		timer.cancel();
		taskManager.stopTasks();
	}


	public void calculationCouldntBeDone() 
	{
		userInterface.calculationErrorDialogShower();
		
	}
	

}
