package watki_kalkulator;


public class OperarationDataCantainer 
{
	private OperationOption whichOperationChoosen = null;
	private double firstNumber;
	private double sumNumber;
	private boolean isAfterCalculation = false;
	
	public OperarationDataCantainer(OperationOption whichOperationChoosen, double firstNumber, double secondNumber)
	{
		super();
		this.whichOperationChoosen = whichOperationChoosen;
		this.firstNumber = firstNumber;
		this.sumNumber = secondNumber;
	}
	public OperationOption getWhichOperationChoosen()
	{
		return whichOperationChoosen;
	}
	public double getFirstNumber() 
	{
		return firstNumber;
	}
	public double getSumNumber() 
	{
		return sumNumber;
	}
	
	public void setSumNumber(double sumNumber) {
		this.sumNumber = sumNumber;
	}
	
	public boolean isAfterCalculation() 
	{
		return isAfterCalculation;
	}
	public void setAfterCalculation(boolean isAfterCalculation) 
	{
		this.isAfterCalculation = isAfterCalculation;
	}
	

}
