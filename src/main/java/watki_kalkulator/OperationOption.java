package watki_kalkulator;

public enum OperationOption 
{	
	ADDING(0), SUBSTRUCTING(1), DIVIDING(2), MULTIPLICATION(3), MODULO(4);
	
	public final static int maxValue = 4;
	
	private final int value;
	
	private OperationOption(int value) {
		this.value = value;
	}
	
	
	OperationOption getValueByNumber(int value) 
	{
		for(OperationOption item: OperationOption.values()) 
		{
			if(item.value == value) 
			{
				return item;
			}
		}
		return ADDING;
	}
}