package watki_kalkulator;

import javafx.application.Application;
import javafx.stage.Stage;
import watki_kalkulator.task_classes.TaskManager;

public class StartingClass extends Application {
	UIClass uiClass = null;
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		uiClass = new UIClass(primaryStage);
	}

	 public static void main(String[] args)
	 {
		 	
	        launch(args); // uruchomienie aplikacji
	 }

	@Override
	public void stop() throws Exception 
	{
		TaskManager.setApplicationRunning(false);		
		super.stop();		
	}
	 
	
}
