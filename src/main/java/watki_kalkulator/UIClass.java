package watki_kalkulator;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class UIClass  {
	
	private Stage primaryStage;
	
	private Label actualSumLabel;
	private Label lastOperationSign;
	private Label lastOperationNumber; 
	private Label timerLabel;
	BackGroundManager backGroundManager;
	
	public UIClass(Stage primaryStage) {
	     this.primaryStage = primaryStage;
	     
	     GridPane gridPane = new GridPane();
	     gridPane.setHgap(4);
	     gridPane.setVgap(10);
	    // gridPane.setStyle("-fx-background-color:red");

	     Label sumNumberLabel = new Label("Suma działań:");
	     gridPane.add(sumNumberLabel, 0, 0);
	     
	     actualSumLabel = new Label();
	     gridPane.add(actualSumLabel, 1, 0);
	     
	     Label lastOperationLabel = new Label("Znak ostatniej operacji:");
	     gridPane.add(lastOperationLabel, 0, 1);
	     
	     lastOperationSign = new Label();
	     gridPane.add(lastOperationSign, 1, 1);
	    
	     Label lastNumberLabel = new Label("Czynnik ostatniej operacji:");
	     gridPane.add(lastNumberLabel, 0, 2);
	     
	     lastOperationNumber = new Label();
	     gridPane.add(lastOperationNumber, 1, 2);
	     
	     Label counterLabel = new Label("Do kolejnego losowania:");
	     gridPane.add(counterLabel, 0, 3);
	     
	     timerLabel = new Label();
	     gridPane.add(timerLabel, 1, 3);
	     
	     Scene myScene = new Scene(gridPane, 600,300);
	     primaryStage.setScene(myScene);
	     primaryStage.show();
	     setOnCloseListener();
	     setStartingValues();
	     
	}
	
	private void setStartingValues()
	{
		actualSumLabel.setText("1");
		lastOperationSign.setText("");
		lastOperationNumber.setText("");
		timerLabel.setText("");
		backGroundManager = new BackGroundManager(this);
	}
	
	public void updateTimer(int timeToSet)
	{
		timerLabel.setText(Integer.toString(timeToSet));
	}
	
	
	
	public void updateNumbers(OperarationDataCantainer operarationDataCantainer)
	{
		actualSumLabel.setText(Double.toString(operarationDataCantainer.getSumNumber()));
		lastOperationSign.setText(operarationDataCantainer.getWhichOperationChoosen().toString());
		lastOperationNumber.setText(Double.toString(operarationDataCantainer.getFirstNumber()));
	}
	
	public double getSumNumber()
	{
		return Double.parseDouble(actualSumLabel.getText());
	}
	 
	public void setOnCloseListener()
	{
		primaryStage.setOnHiding(new EventHandler<WindowEvent>() {

            @Override
            public void handle(WindowEvent event) {
                Platform.runLater(new Runnable() {

                    @Override
                    public void run() {
                        System.out.println("Application Closed by click to Close Button(X)");
                        backGroundManager.stopApplicationTasks();
                    }
                });
            }
        });
	}
	
	public void calculationErrorDialogShower()
	{
		Platform.runLater(new Runnable() {
		

			@Override
			public void run() {
				// TODO Auto-generated method stub	
				Alert alert = new Alert(Alert.AlertType.ERROR);
				alert.setTitle("Uwaga");
				alert.setHeaderText("Nie mogliśmy dokonać oblczeń");
				alert.setContentText("Obliczenia za trudne dla nasS");
				alert.showAndWait();
			}
			
		});
	}	
}
