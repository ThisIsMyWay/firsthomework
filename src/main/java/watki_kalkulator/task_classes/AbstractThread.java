package watki_kalkulator.task_classes;

import watki_kalkulator.OperarationDataCantainer;
import watki_kalkulator.OperationOption;

public abstract class AbstractThread implements Runnable
{
	
	protected abstract OperationOption getMyOperation();	
	
	protected abstract void makeCalculation(OperarationDataCantainer operarationDataCantainer);
	
	OperarationDataCantainer operarationDataCantainer;
	 
	private TaskManager taskManager = null;
	
	private AbstractThread threadToWhichPassIfWeNotCalculatedIt = null;
	
	private boolean shouldRunning = true;
	
	
	
	public AbstractThread(TaskManager taskManager, AbstractThread threadToWhichPassIfWeNotCalculatedIt) {
		super();
		this.taskManager = taskManager;
		this.threadToWhichPassIfWeNotCalculatedIt = threadToWhichPassIfWeNotCalculatedIt;
	}

	@Override
	public void run() 
	{
			while(shouldRunning)
			{
				synchronized(this)
				{
					if (operarationDataCantainer == null)
					{
						try 
						{
							wait();
						} 
						catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					else 
					{
						tryToMakeCalculation();
					}							
				}
			}
	}
	
	public void releaseTaskToCalculate(OperarationDataCantainer operarationDataCantainer)
	{
		synchronized(this)
		{
			this.operarationDataCantainer = operarationDataCantainer;
			this.notify();
		}
	}
	
	public void releaseTaskToFinish()
	{
		synchronized (this) 
		{
			this.shouldRunning = false;
			this.notify();
		}
	}
	
	private void tryToMakeCalculation() 
	{
		if (checkIfIHaveToDoThisOperation(operarationDataCantainer))
		{
			makeCalculation(operarationDataCantainer);
			calculationDoneTaskManagerNotifier(operarationDataCantainer);			
		}
		else 
		{
			passCalculationToNextThread(operarationDataCantainer);			
		}		
		operarationDataCantainer = null;
	}
	
	private void passCalculationToNextThread(OperarationDataCantainer operarationDataCantainer) 
	{
		if (threadToWhichPassIfWeNotCalculatedIt != null)
		{
			threadToWhichPassIfWeNotCalculatedIt.releaseTaskToCalculate(operarationDataCantainer);
		}
		taskManager.calculationCouldntBeDone();
	}

	private boolean checkIfIHaveToDoThisOperation(OperarationDataCantainer operarationDataCantainer)
	{
		return operarationDataCantainer.getWhichOperationChoosen() == getMyOperation();
	}
	
	
	public void calculationDoneTaskManagerNotifier(OperarationDataCantainer operarationDataCantainer)
	{
		taskManager.calculationDone(operarationDataCantainer);
	}
	
}
