package watki_kalkulator.task_classes;

import watki_kalkulator.OperarationDataCantainer;
import watki_kalkulator.OperationOption;

public class AdditionThread extends AbstractThread {

	public AdditionThread(TaskManager taskManager, AbstractThread threadToWhichPassIfWeNotCalculatedIt) 
	{
		super(taskManager, threadToWhichPassIfWeNotCalculatedIt);
	}

	@Override
	protected OperationOption getMyOperation() {
		// TODO Auto-generated method stub
		return OperationOption.ADDING;
	}

	@Override
	protected void makeCalculation(OperarationDataCantainer operarationDataCantainer) {

		operarationDataCantainer.setSumNumber(operarationDataCantainer.getFirstNumber() + operarationDataCantainer.getSumNumber());
		operarationDataCantainer.setAfterCalculation(true);
	}

}
