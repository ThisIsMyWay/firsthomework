package watki_kalkulator.task_classes;

import watki_kalkulator.OperarationDataCantainer;
import watki_kalkulator.OperationOption;

public class DivisionThread extends AbstractThread {

	public DivisionThread(TaskManager taskManager, AbstractThread threadToWhichPassIfWeNotCalculatedIt) {
		super(taskManager, threadToWhichPassIfWeNotCalculatedIt);
	}

	@Override
	protected OperationOption getMyOperation() {
		// TODO Auto-generated method stub
		return OperationOption.DIVIDING;
	}

	@Override
	protected void makeCalculation(OperarationDataCantainer operarationDataCantainer) {

		operarationDataCantainer.setSumNumber(operarationDataCantainer.getSumNumber() / operarationDataCantainer.getFirstNumber());
		operarationDataCantainer.setAfterCalculation(true);
	}

}
