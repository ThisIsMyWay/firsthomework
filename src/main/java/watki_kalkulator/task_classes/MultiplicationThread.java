package watki_kalkulator.task_classes;

import watki_kalkulator.OperarationDataCantainer;
import watki_kalkulator.OperationOption;

public class MultiplicationThread extends AbstractThread {

	public MultiplicationThread(TaskManager taskManager, AbstractThread threadToWhichPassIfWeNotCalculatedIt) {
		super(taskManager, threadToWhichPassIfWeNotCalculatedIt);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected OperationOption getMyOperation() {
		// TODO Auto-generated method stub
		return OperationOption.MULTIPLICATION;
	}

	@Override
	protected void makeCalculation(OperarationDataCantainer operarationDataCantainer) {

		operarationDataCantainer.setSumNumber(operarationDataCantainer.getFirstNumber() * operarationDataCantainer.getSumNumber());
		operarationDataCantainer.setAfterCalculation(true);
	}

}
