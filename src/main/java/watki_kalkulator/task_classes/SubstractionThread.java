package watki_kalkulator.task_classes;

import watki_kalkulator.OperarationDataCantainer;
import watki_kalkulator.OperationOption;

public class SubstractionThread extends AbstractThread {

	public SubstractionThread(TaskManager taskManager, AbstractThread threadToWhichPassIfWeNotCalculatedIt) {
		super(taskManager, threadToWhichPassIfWeNotCalculatedIt);
	}

	@Override
	protected OperationOption getMyOperation() 
	{
		return OperationOption.SUBSTRUCTING;
	}

	@Override
	protected void makeCalculation(OperarationDataCantainer operarationDataCantainer) {

		operarationDataCantainer.setSumNumber(operarationDataCantainer.getSumNumber() - operarationDataCantainer.getFirstNumber());
		operarationDataCantainer.setAfterCalculation(true);
	}

}
