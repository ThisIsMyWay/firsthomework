package watki_kalkulator.task_classes;



import java.util.ArrayList;
import java.util.List;

import watki_kalkulator.BackGroundManager;
import watki_kalkulator.OperarationDataCantainer;

public class TaskManager
{
	private static boolean applicationRunning = true;
	AdditionThread additionThread = null;
	BackGroundManager backGroundManager = null;
	List<AbstractThread> listOfCalculationThreads ;

	public TaskManager(BackGroundManager backGroundManager) {
		super();
		this.backGroundManager = backGroundManager;
		createAndStartAllTask();
	}

	private void createAndStartAllTask() 
	{
		createTasksAndAddToList();
	
		startTask();
	}

	private void createTasksAndAddToList() {
		listOfCalculationThreads = new ArrayList<>();
		ModuloThread moduloThread = new ModuloThread(this, null);
		listOfCalculationThreads.add(moduloThread);
		MultiplicationThread multiplicationThread = new MultiplicationThread(this, moduloThread);
		listOfCalculationThreads.add(multiplicationThread);
		DivisionThread divisionThread = new DivisionThread(this, multiplicationThread);
		listOfCalculationThreads.add(divisionThread);
		SubstractionThread substractionThread = new SubstractionThread(this, divisionThread);
		listOfCalculationThreads.add(substractionThread);
		additionThread = new AdditionThread(this, substractionThread);
		listOfCalculationThreads.add(additionThread);
	}

	private void startTask()
	{
		for (AbstractThread threadToStart : listOfCalculationThreads)
		{
			new Thread(threadToStart).start();
		}		
	}
	
	public void stopTasks()
	{
		for (AbstractThread threadToStart : listOfCalculationThreads)
		{
			threadToStart.releaseTaskToFinish();
		} 
	}

	public static boolean isApplicationRunning() {
		return applicationRunning;
	}

	public static void setApplicationRunning(boolean applicationRunning) {
		TaskManager.applicationRunning = applicationRunning;
	}

	public void startCalculation(OperarationDataCantainer operarationDataCantainer)
	{
		additionThread.releaseTaskToCalculate(operarationDataCantainer);
	}
	
	public void calculationDone(OperarationDataCantainer operarationDataCantainer) 
	{
		backGroundManager.setOperationResult(operarationDataCantainer);
	}
	
	public void calculationCouldntBeDone() 
	{
		backGroundManager.calculationCouldntBeDone();
		
	}
	
}
